#!/usr/bin/env ruby
# Encoding: utf-8

require 'time'
require 'mail'
require 'net/imap'
require 'elasticsearch'
require 'json'
require 'fileutils'
require 'logger'
require 'dotenv/load'

$PATH = '/thoth'
$INDEX = 'thoth'
$LOG = Logger.new($PATH+'/thoth.log', 'monthly')

# flag to print Ruby library debug info (very detailed)
@net_imap_debug = false

# script application level debug flag
@debug = true

# set true to delete mail after processing 
@expunge = false

# return timestamp in ISO8601 with precision in milliseconds 
def time_now
  Time.now.utc.iso8601(3)
end

#
# imap_connection  
#
# connect to a specified serve and login
#
def imap_connection(server, username, password)
  # connect to IMAP server
  imap = Net::IMAP.new server, ssl: false

  Net::IMAP.debug = @net_imap_debug

  # http://ruby-doc.org/stdlib-2.1.5/libdoc/net/imap/rdoc/Net/IMAP.html#method-i-capability
  capabilities = imap.capability 

  $LOG.debug "imap capabilities: #{capabilities.join(',')}" if @debug

  unless capabilities.include? "IDLE"
    $LOG.fatal "'IDLE' IMAP capability not available in server: #{server}"
    imap.disconnect
    exit
  end

  # login
  imap.authenticate 'LOGIN', username, password

  # return IMAP connection handler
  imap
end


#
# retrieve_emails
#
# retrieve any mail from a folder, followin specified serach condition
# for any mail retrieved call a specified block
#
def retrieve_emails(imap, search_condition, folder, &process_email_block)
  $LOG.info "\nsearch messages"
  # select folder
  imap.select folder

  # search messages that satisfy condition
  message_ids = imap.search(search_condition) 

  if @debug
    if message_ids.empty?
      $LOG.info "no messages found."
      return
    else
      $LOG.info "#{message_ids.count} messages processed."
    end    
  end

  message_ids.each do |message_id|

    # fetch all the email contents
    msg = imap.fetch(message_id,'RFC822')[0].attr['RFC822']
    
    # instantiate a Mail object to avoid further IMAP parameters nightmares 
    mail = Mail.read_from_string msg

    # call the block with mail object as param 
    process_email_block.call mail
    
    # mark as read ore deleted
    if @expunge
      imap.store(message_id, "+FLAGS", [:Deleted])
    else
      imap.store(message_id, "+FLAGS", [:Seen])
    end
  end
end  


#
# process_mail
#
# do something with the e-mail content (param is a Mail gem instance)
#
def process_email(mail)
  #
  # just puts to stdout 
  #
  #puts mail
  sender = mail.from
  recipient = mail.to
  cc = mail.cc
  subject = mail.subject
  message_id = mail.message_id
  references = mail.references
  begin
    if mail.multipart?
      part = mail.html_part || mail.text_part || mail
      encoding = part.content_type_parameters['charset'] if part.content_type_parameters
      # get the message body without the header information
      body = part.body.decoded
      #` convert it to UTF-8
      body = body.force_encoding(encoding).encode('UTF-8') if encoding
    else
      body = mail.body.decoded
    end
  rescue
    body = "Can't read body"
  end


  #puts (mail.text_part.body.to_s).green

  $LOG.info "new mail ID: "
  $LOG.info " message id: #{mail.message_id}" 
  #puts "intern.date: #{mail.date.to_s}"       
  $LOG.info "    from: #{sender}"
  $LOG.info "    subject: #{subject}"

#  $LOG.info "folders:"
#  folders.each do | folder |
#   $LOG.info "@{folder}"
#  end

attachments = []
time = Time.now
  #puts "  text body: #{body[0..80]}#{(body.size > 80) ? '...': ''}"
  mail.attachments.each do | attachment |
    # Attachments is an AttachmentsList object containing a
    # number of Part objects
    o = [('a'..'z')].map { |i| i.to_a }.flatten
    name = (0...32).map { o[rand(o.length)] }.join
    path = File.join($PATH, 'storage', time.year.to_s, time.month.to_s.rjust(2, '0'), time.day.to_s.rjust(2, '0'))
    file = File.join(path, name)
    FileUtils.mkdir_p path unless File.exists?(path)
    begin
      File.open(file, "w+b", 0644) {|f| f.write attachment.body.decoded}
    rescue => e
      puts "Unable to save data for #{file} because #{e.message}"
    end
    attachments.push({
      :filename => attachment.filename,
      :path => file
    })
  end

  client = Elasticsearch::Client.new log: true
  client.index index: $INDEX,
  type: 'document',
  body: {
    subject: subject,
    body: body.force_encoding("UTF-8"),
    mail_from: sender,
    date: time.year.to_s + '-' + time.month.to_s.rjust(2, '0') + '-' + time.day.to_s.rjust(2, '0'),
    mail_to: recipient,
    attachments: attachments,
    cc: cc,
    type: 'document',
    references: "#{references} #{message_id}",
    mail_date: mail.date.to_s
  }

end


def shutdown(imap)
  imap.idle_done
  imap.logout unless imap.disconnected?
  imap.disconnect
  
  $LOG.info "#{$0} has ended (crowd applauds)"
  exit 0
end

#
# idle_loop
#
# check for any further mail with "real-time" responsiveness.
# retrieve any mail from a folder, following specified search condition
# for any mail retrieved call a specified block
#
def idle_loop(imap, search_condition, folder, server, username, password)

  $LOG.info "waiting new mails (IDLE loop)..."

  # http://stackoverflow.com/questions/4611716/how-imap-idle-works
  loop do
    begin
      imap.select folder
      imap.idle do |resp|

        # You'll get all the things from the server. For new emails (EXISTS)
        if resp.kind_of?(Net::IMAP::UntaggedResponse) and resp.name == "EXISTS"

          $LOG.debug resp.inspect if @debug
          # Got something. Send DONE. This breaks you out of the blocking call
          imap.idle_done
        end
      end

      # We're out, which means there are some emails ready for us.
      # Go do a search for UNSEEN and fetch them.
      retrieve_emails(imap, search_condition, folder) { |mail| process_email mail }

      # delete processed mails (or just flah them as "seen" ) 
      imap.expunge if @expunge

    rescue SignalException => e
      # http://stackoverflow.com/questions/2089421/capturing-ctrl-c-in-ruby
      $LOG.info "Signal received at #{time_now}: #{e.class}. #{e.message}"
      shutdown imap

    rescue Net::IMAP::Error => e
      $LOG.info "Net::IMAP::Error at #{time_now}: #{e.class}. #{e.message}"

      # timeout ? reopen connection
      imap = imap_connection(server, username, password) #if e.message == 'connection closed'
      $LOG.info "reconnected to server: #{server}"

    rescue Exception => e
      $LOG.error "Something went wrong at #{time_now}: #{e.class}. #{e.message} #{e.backtrace}"

      imap = imap_connection(server, username, password)
      $LOG.info "reconnected to server: #{server}"
    end
  end
end


#
# main
#
# get parameters from environment variables
#
server = ENV['IMAP_SERVER']
username = ENV['USERNAME']
password = ENV['PASSWORD']
folder = ENV['FOLDER']
search_condition = ['UNSEEN']

if !server or !password or !username or !folder
  puts "specify IMAP_SERVER, USERNAME, PASSWORD and FOLDER in .env file"
  exit
end

$LOG.info "         imap server: #{server}"
$LOG.info "         username: #{username}"
$LOG.info "           folder: #{folder}"
$LOG.info " search condition: #{search_condition.join(',')}"

imap = imap_connection(server, username, password)

# at start-up check for any mail (already received) and process them 
retrieve_emails(imap, search_condition, folder) { |mail| process_email mail }

# check for any further mail with "real-time" responsiveness
idle_loop(imap, search_condition, folder, server, username, password)

imap.logout
imap.disconnect
